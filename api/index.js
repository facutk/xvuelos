const express = require('express');
const fetch = require("node-fetch");
const cors = require("cors");
const qs = require('qs');

const getClientIp = require('./util/getClientIp');
const getCountryCurrency = require('./util/getCountryCurrency');

const { SKYSCANNER_API_KEY, SKYSCANNER_URL, PORT = 3000 } = process.env;

const app = express();
app.disable('x-powered-by');
app.use(cors({ origin: true }));

app.get('/', (req, res) => res.sendStatus(200));
app.get('/status', (req, res) => res.json({ status: 'ok' }));
app.get('/ip', (req, res) => res.send(getClientIp(req)));

app.get('/userinfo', async (req, res) => {
  const ip = getClientIp(req);
  
  const response = await fetch(`${SKYSCANNER_URL}/autosuggest/v1.0/US/USD/en?id=${ip}-ip&apiKey=${SKYSCANNER_API_KEY}`);
  const { Places = [] } = await response.json();
  const [location] = Places;
  const {
    PlaceId,    // BUEA-sky
    PlaceName,  // Buenos Aires
    CountryId,  // AR-sky
    CityId,     // BUEA-sky
    CountryName // Argentina
  } = location;

  const locale = 'es-MX';
  const market = CountryId.split('-')[0];
  const currency = getCountryCurrency(CountryName);
  
  const userInfo = {
    locale,
    market,
    currency
  };

  return res.status(200).json(userInfo);
});

app.get('/locales', async (req, res) => {
  const response = await fetch(`${SKYSCANNER_URL}/reference/v1.0/locales?apiKey=${SKYSCANNER_API_KEY}`);
  const { Locales = [] } = await response.json();

  return res.status(200).json(Locales);
});

app.get('/currencies', async (req, res) => {
  const response = await fetch(`${SKYSCANNER_URL}/reference/v1.0/currencies?apiKey=${SKYSCANNER_API_KEY}`);
  const { Currencies = [] } = await response.json();

  return res.status(200).json(Currencies.map(({ Code, Symbol }) => ({ Code, Symbol })));
});

app.get('/markets/:locale', async (req, res) => {
  const { locale } = req.params;

  const response = await fetch(`${SKYSCANNER_URL}/reference/v1.0/countries/${locale}?apiKey=${SKYSCANNER_API_KEY}`);
  const { Countries = [] } = await response.json();

  return res.status(200).json(Countries);
});

app.get('/autosuggest/:country/:currency/:locale', async (req, res) => {
  const { country, currency, locale } = req.params;
  const { query } = req.query;

  const response = await fetch(`${SKYSCANNER_URL}/autosuggest/v1.0/${country}/${currency}/${locale}?query=${query}&apiKey=${SKYSCANNER_API_KEY}`);
  const { Places = [] } = await response.json();

  return res.status(200).json(Places);
});

app.get('/session/:country/:currency/:locale/:originPlace/:destinationPlace/:outboundDate/:inboundDate', async (req, res) => {
  const { country, currency, locale, originPlace, destinationPlace, outboundDate, inboundDate } = req.params;
  const {
    adults = '1',
    children = '0',
    infants = '0',
    cabinClass = 'economy',
    locationSchema = 'iata',
    groupPricing = 'false'
  } = req.query;

  const params = {
    country,
    currency,
    locale,
    adults,
    children,
    infants,
    locationSchema,
    originPlace,
    cabinClass,
    destinationPlace,
    outboundDate,
    inboundDate,
    groupPricing,
    apikey: SKYSCANNER_API_KEY
  };

  const r = await fetch(`${SKYSCANNER_URL}/pricing/v1.0`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    body: qs.stringify(params)
  });

  const location = r.headers.get('location');
  const pollingUrl = `${location}/?apiKey=${SKYSCANNER_API_KEY}&pageIndex=0&pageSize=1"`;
  const rPolling = await fetch(pollingUrl);
  const responseJson = await rPolling.json();
  
  return res.status(200).json(responseJson);
});

app.get('/poll', async (req, res) => {
  const { sessionKey } = req.query;
  
  const r = await fetch(`${SKYSCANNER_URL}/pricing/uk1/v1.0/${sessionKey}?apiKey=${SKYSCANNER_API_KEY}`);
  const responseJson = await r.json();

  return res.status(200).json(responseJson);
});

app.listen(PORT, () => {
  if (!SKYSCANNER_API_KEY) {
    console.error('SKYSCANNER_API_KEY is not defined!');
  }

  console.log(`App listening on port ${PORT}`);
});
