const getClientIp = (req) => {
  if (process.env.MOCK_IP_ADDRESS) {
    return process.env.MOCK_IP_ADDRESS;
  }

  if (req.header('x-forwarded-for')) {
    return req.header('x-forwarded-for').split(',')[0];
  }
  
  return req.connection.remoteAddress;
};

module.exports = getClientIp;
