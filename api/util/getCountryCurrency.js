const countryCurrencies = require('country-json/src/country-by-currency-code.json');

const getCountryCurrency = (CountryName) => {
  let currency = 'USD';

  const countryCurrency = countryCurrencies.find(country => country.country === CountryName);
  if (countryCurrency) {
    currency = countryCurrency.currency_code;
  };

  return currency;
};

module.exports = getCountryCurrency;
