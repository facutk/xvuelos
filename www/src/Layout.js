import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  border: 1px solid green;
  height: 300px;
  display: flex;
  flex-direction: row;
  @media (max-width: 450px) {
    flex-direction: column;
  }
`;

const Filters = styled.div`
  display: flex;
  border: 1px solid red;
  height: 300px;
  flex: 1;
`;

const Flights = styled.div`
  display: flex;
  border: 1px solid blue;
  height: 300px;
  flex: 3;
`;

const Layout = () => (
  <Container>
    <Filters />
    <Flights />
  </Container>
);

export default Layout;
