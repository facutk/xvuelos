import React from 'react';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components';
import { Button } from 'baseui/button';
import { Datepicker } from 'baseui/datepicker';
import dayjs from 'dayjs';
import 'dayjs/locale/es';
import SearchIcon from 'baseui/icon/search';

import { ResponsiveContainer } from './components/Layout';
import SearchBox from './SearchBox';
import { dateDiff } from './util';
import { useDatesLocalStorage, useLocalStorage } from './hooks';

const SelectWrapper = styled.div`
  width: calc(100% - 16px);
  margin: 6px 8px;
  font-family: Lato;
  font-size: 0.75em;
`;

const parsePlace = (place = '') => place.split('-')[0];

const dateToYMD = day => dayjs(day).format('YYYY-MM-DD');

const genFlightsLink = ([origin = {}], [destination = {}], departureDay, returnDay) => {
  const originId = parsePlace(origin.PlaceId);
  const destinationId = parsePlace(destination.PlaceId);
  const departureDayStr = dateToYMD(departureDay);
  const returnDayStr = dateToYMD(returnDay);

  const link = `/vuelos/${originId}/${destinationId}/${departureDayStr}/${returnDayStr}`;

  return originId.length && destinationId.length && departureDay && returnDay && link;
};

const today = new Date();

const SearchForm = () => {
  const history = useHistory();
  const [origin, setOrigin] = useLocalStorage(
    'origin', []
  );
  const [destination, setDestination] = useLocalStorage(
    'destination', []
  );

  const [dates, setDates] = useDatesLocalStorage('dates');
  const [departureDate, returnDate] = dates;

  const link = genFlightsLink(origin, destination, departureDate, returnDate);

  const nightCount = dateDiff(departureDate, returnDate);

  return (
    <>
      <ResponsiveContainer>
        <SelectWrapper>
          Origen
          <SearchBox
            value={origin}
            onChange={setOrigin}
            placeholder='Ingresá desde dónde viajas'
          />
        </SelectWrapper>
        <SelectWrapper>
          Destino
          <SearchBox
            value={destination}
            onChange={setDestination}
            placeholder='Ingresá hacia dónde viajas'
          />
        </SelectWrapper>
      </ResponsiveContainer>

      <ResponsiveContainer>
        <SelectWrapper>
          {nightCount === null ?
            'Fechas' :
            `${nightCount} Noche${nightCount === 1 ? '' : 's'}`
          }
          <Datepicker
            value={dates}
            onChange={({ date }) => setDates(date)}
            minDate={today}
            range
          />
        </SelectWrapper>

        <SelectWrapper>
          {'\u00a0'}
          <Button
            disabled={!link}
            onClick={() => history.push(link)}
            style={{ width: '100%' }}
            startEnhancer={SearchIcon}
          >
            Buscar Vuelos
          </Button>
        </SelectWrapper>
      </ResponsiveContainer>
    </>
  );
}

export default SearchForm;
