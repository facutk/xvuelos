import mockSession from './mockSession.json';
import { shorten } from '../util';

const BASE_URL = process.env.REACT_APP_LOCAL_API || 'https://api.xvuelos.com';

const LOCALES_URL = `${BASE_URL}/locales`;
const CURRENCIES_URL = `${BASE_URL}/currencies`;
const MARKETS_URL = `${BASE_URL}/markets`;
const USER_LOCATION_URL = `${BASE_URL}/location`;
const AUTOSUGGEST_URL = `${BASE_URL}/autosuggest`;
const SESSION_URL = `${BASE_URL}/session`;
const POLL_URL = `${BASE_URL}/poll`;
const USERINFO_URL = `${BASE_URL}/userinfo`;

export const getUserInfo = () => fetch(USERINFO_URL).then(r => r.json());
export const getLocales = () => fetch(LOCALES_URL).then(r => r.json());
export const getCurrencies = () => fetch(CURRENCIES_URL).then(r => r.json());
export const getMarkets = locale => fetch(`${MARKETS_URL}/${locale}`).then(r => r.json());
export const getUserLocation = () => fetch(USER_LOCATION_URL).then(r => r.json());
export const getAutosuggest = (country, currency, locale, query) =>
  fetch(`${AUTOSUGGEST_URL}/${country}/${currency}/${locale}?query=${query}`)
  .then(response => response.json())
  .then(results => results.map(result => ({
    ...result,
    label: `${result.PlaceName}, ${result.CountryName}`
  })));

export const getSessionMock = ({ Status = 'UpdatesPending' }) => new Promise((resolve) => {
  setTimeout(() => resolve({ ...mockSession, Status }), 250);
});

const shortenItineraryId = Itinerary => ({
  ...Itinerary,
  id: shorten(Itinerary.BookingDetailsLink.Body)
})

const shortenItineraryIds = response => ({
  ...response,
  Itineraries: response.Itineraries.map(shortenItineraryId)
});

export const getSession = ({
  market,
  currency,
  locale,
  originPlace,
  destinationPlace,
  outboundDate,
  inboundDate,
  adults = '1',
  children = '0',
  infants = '0',
  cabinClass = 'economy',
  locationSchema = 'iata',
  groupPricing = 'false',
  isDebug = false
}) => {
  if (isDebug) {
    return getSessionMock({}).then(shortenItineraryIds);
  }

  return fetch(`${SESSION_URL}/${market}/${currency}/${locale}/${originPlace}-sky/${destinationPlace}-sky/${outboundDate}/${inboundDate}?adults=${adults}&children=${children}&infants=${infants}&cabinClass=${cabinClass}&locationSchema=${locationSchema}&groupPricing=${groupPricing}`)
  .then(response => response.json())
  .then(shortenItineraryIds);
}

export const getPollResults = ({
  SessionKey,
  isDebug
}) => {
  if (isDebug) {
    return getSessionMock({ Status: 'UpdatesComplete' }).then(shortenItineraryIds);
  }

  return fetch(`${POLL_URL}?sessionKey=${SessionKey}`)
    .then(response => response.json())
    .then(shortenItineraryIds);
};
