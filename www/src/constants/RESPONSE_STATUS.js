export const RESPONSE_STATUS = {
  UPDATES_PENDING: 'UpdatesPending',
  UPDATES_COMPLETE: 'UpdatesComplete',
};
