export const USERINFO_DEFAULT = {
  locale: 'es-MX',
  market: 'CO',
  currency: 'USD',
  mustRefresh: true
};
