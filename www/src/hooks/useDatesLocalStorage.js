import { useEffect } from 'react';
import { useLocalStorage } from '../hooks';

const today = new Date();
today.setHours(0, 0, 0, 0);

const useDatesLocalStorage = (key) => {
  const [datesSaved, setDates] = useLocalStorage(key, [today, today]);

  const [departureDay, returnDay] = datesSaved;

  const dates = [];
  let departureDate;

  if (departureDay) {
    departureDate = new Date(departureDay);
    departureDate.setHours(0, 0, 0, 0);
    dates.push(departureDate);
  }

  let returnDate;
  if (returnDay) {
    returnDate = new Date(returnDay);
    returnDate.setHours(0, 0, 0, 0);
    dates.push(returnDate);
  }

  useEffect(() => {
    // make sure stored dates make sense
    if (departureDate < today) {
      setDates([today, today]);
    }
  }, [departureDate, setDates, datesSaved]);

  return [dates, setDates];
}

export default useDatesLocalStorage;
