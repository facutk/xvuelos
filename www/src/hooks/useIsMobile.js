import { useMediaQuery } from '../hooks';
import { MOBILE_BREAKPOINT } from '../constants';

const useIsMobile = () => {
  return useMediaQuery(MOBILE_BREAKPOINT);
};

export default useIsMobile;
