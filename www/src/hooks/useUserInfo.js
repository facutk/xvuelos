import { useEffect } from 'react';

import { useLocalStorage } from '../hooks';
import { getUserInfo } from '../api';
import { USERINFO_DEFAULT } from '../constants';

const useUserInfo = () => {
  const [userInfo, setUserInfo] = useLocalStorage(
    'userInfo', USERINFO_DEFAULT
  );

  useEffect(() => {
    if (userInfo.mustRefresh) {
      getUserInfo().then(setUserInfo);
    }
  }, [setUserInfo, userInfo.mustRefresh]);

  return [userInfo, setUserInfo];
};

export default useUserInfo;
