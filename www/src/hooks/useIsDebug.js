import { useLocalStorage } from '../hooks';

const useIsDebug = () => {
  const [isDebug, setIsDebug] = useLocalStorage(
    'isDebug', false
  );

  return [isDebug, setIsDebug];
};

export default useIsDebug;
