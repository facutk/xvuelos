import React from 'react';
import { Checkbox } from 'baseui/checkbox';

import { useIsDebug, useUserInfo } from './hooks';

const Debug = () => {
  const [isDebug, setIsDebug] = useIsDebug();
  const [userInfo] = useUserInfo();

  return (
    <React.Fragment>
      <Checkbox checked={isDebug} onChange={() => setIsDebug(!isDebug)}>
        Debug mode
      </Checkbox>
      <pre>
        {JSON.stringify(userInfo, undefined, 2)}
      </pre>
    </React.Fragment>
  );
}

export default Debug;
