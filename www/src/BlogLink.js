import React from 'react';
import styled from 'styled-components';

const StyledLink = styled.a`
  text-transform: uppercase;
  font-size: 12px;
  letter-spacing: 1px;
  text-decoration: none;
  color: #276EF1;
  line-height: 21px;
`;

const BlogLink = () => (
  <StyledLink href="https://www.xvuelos.com/destinos/" alt="Destinos" target="_blank">
    Destinos
  </StyledLink>
);

export default BlogLink;
