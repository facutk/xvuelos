const minsToHHMM = (mins = 0) => {
  const hours = Math.floor(mins / 60);          
  const minutes = mins % 60;
  const zeroPadMinute = minutes > 9 ? '' : '0';

  return `${hours}h ${zeroPadMinute}${minutes}m`;
}

export default minsToHHMM;
