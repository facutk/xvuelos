import md5 from 'md5';

const shorten = (str) => md5(str).slice(-8);

export default shorten;
