import dayjs from 'dayjs';

const datetimeToHHMM = datetime => dayjs(datetime).format('HH:mm');

export default datetimeToHHMM;
