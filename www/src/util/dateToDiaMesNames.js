const monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
  "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
];
const daysNames =["Dom.","Lun.","Mar.","Miér.","Jue.","Vier.","Sáb."]

const formatDate = (date) => {

    const d = new Date(date);
    
    return  daysNames[d.getDay()] + "  " + d.getDate() + ` de ${monthNames[d.getMonth()]} de ` + d.getFullYear() ;

}


export default formatDate;