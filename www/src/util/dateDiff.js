const MS_PER_DAY = 1000 * 60 * 60 * 24;

const dateDiff = (fromDay, toDay) => {
  if (!fromDay || !toDay) return null;

  const diffTime = Math.abs(toDay.getTime() - fromDay.getTime());
  const diffDays = Math.ceil(diffTime / MS_PER_DAY);
  return diffDays;
}

export default dateDiff;
