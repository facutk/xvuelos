import React, { useState } from 'react';
import { Select, SIZE } from 'baseui/select';
import { StyledSpinnerNext as Spinner } from 'baseui/spinner';
import { ResponsiveContainer } from './components/Layout';

import { getCurrencies, getMarkets } from './api';
import { useUserInfo } from './hooks';

const UserInfo = () => {
  const [userInfo, setUserInfo] = useUserInfo();
  const [markets, setMarkets] = useState([]);
  const [marketsLoading, setMarketsLoading] = useState(false);

  const [currencies, setCurrencies] = useState([]);
  const [currenciesLoading, setCurrenciesLoading] = useState(false);

  const handleGetMarkets = () => {
    if (markets.length) {
      return;
    }

    setMarketsLoading(true);
    getMarkets(userInfo.locale).then((marketsResponse) => {
      setMarkets(marketsResponse);
      setMarketsLoading(false);
    });
  }

  const handleGetCurrencies = () => {
    if (currencies.length) {
      return;
    }

    setCurrenciesLoading(true);
    getCurrencies().then((currenciesResponse) => {
      setCurrencies(currenciesResponse);
      setCurrenciesLoading(false);
    });
  }

  const handleUserInfoChange = (field, value) => setUserInfo({
    ...userInfo,
    [field]: value
  });

  return (
    <React.Fragment>
      <ResponsiveContainer>
        <div style={{ display: 'inline-block', margin: '0 5px', width: '100%' }} />
        <div
          style={{ display: 'inline-block', margin: '0 5px', width: '100%' }}
        >
          <Select
            options={markets}
            labelKey='Name'
            valueKey='Code'
            onChange={e => handleUserInfoChange('market', e.value[0].Code)}
            noResultsMsg={<Spinner />}
            isLoading={marketsLoading}
            maxDropdownHeight='200px'
            placeholder={`País • ${userInfo.market}`}
            size={SIZE.compact}
            onOpen={handleGetMarkets}
          />
        </div>
        <div style={{ display: 'inline-block', margin: '0 5px', width: '100%' }}>
          <Select
            options={currencies}
            labelKey='Code'
            valueKey='Code'
            onChange={e => handleUserInfoChange('currency', e.value[0].Code)}
            noResultsMsg={<Spinner />}
            isLoading={currenciesLoading}
            maxDropdownHeight='200px'
            placeholder={`Moneda • ${userInfo.currency}`}
            size={SIZE.compact}
            onOpen={handleGetCurrencies}
          />
        </div>
        <div style={{ display: 'inline-block', margin: '0 5px', width: '100%' }} />
      </ResponsiveContainer>
    </React.Fragment>
  );
};

export default UserInfo;
