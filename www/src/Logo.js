import React from 'react';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components';

import { useIsDebug } from './hooks';

const StyledLogo = styled.span`
  cursor: pointer;
  user-select: none;
`;

const Logo = () => {
  const history = useHistory();
  const [ isDebug ] = useIsDebug();

  return (
    <StyledLogo onClick={() => history.push('/')}>
      {isDebug && ('DEBUG')}
      {!isDebug && (
        <img src='/logo.png' alt='X | Vuelos' height='21px' />
      )}
    </StyledLogo>
  )
};

export default Logo;
