import React, { useState, useEffect } from 'react';
import { Select } from 'baseui/select';
import { getAutosuggest } from './api';
import { useDebounce, useUserInfo } from './hooks';

const SearchBox = ({ value, onChange, placeholder }) => {
  const [userInfo] = useUserInfo();
  const [inputText, setInputText] = useState(null);
  const [options, setOptions] = useState([]); // TODO: Grab from localstorage
  const [isLoading, setLoading] = useState(false);
  const debouncedSearchTerm = useDebounce(inputText, 250);

  useEffect(() => {
    if (inputText && inputText.length >= 3) {
      setLoading(true);
      getAutosuggest(userInfo.market, userInfo.currency, 'es-ES', debouncedSearchTerm)
        .then(optionsResponse => {
          setLoading(false);
          setOptions(optionsResponse);
        });
    } else {
      setOptions([]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [debouncedSearchTerm]);

  return (
    <Select
      options={options}
      labelKey='label'
      valueKey='PlaceId'
      value={value}
      onChange={e => onChange(e.value)}
      onInputChange={e => setInputText(e.target.value)}
      isLoading={isLoading}
      noResultsMsg={origin.length && options.length ? (
        <span>No hay resultados</span>
      ) : (
          <span>Ingresa al menos 3 letras</span>
        )}
      maxDropdownHeight='200px'
      placeholder={placeholder}
    />
  );
};

export default SearchBox;
