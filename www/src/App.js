import React from 'react';
import { Helmet } from 'react-helmet';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { StyledSpinnerNext as Spinner } from 'baseui/spinner';

import { Footer, Header, MainContainer, PageContainer, StyleProvider } from './components/Layout';
import Logo from './Logo';
import BlogLink from './BlogLink';
import UserInfo from './UserInfo';
import './style.css';

const Debug = React.lazy(() => import('./Debug'));
const SearchForm = React.lazy(() => import('./SearchForm'));
const Flights = React.lazy(() => import('./Flights'));
const Layout = React.lazy(() => import('./Layout'));
const QuienesSomos = React.lazy(() => import('./components/QuienesSomos'));

const App = () => (
  <StyleProvider>
    <Helmet>
      <meta charSet='utf-8' />
      <meta
        name='viewport'
        content='width=device-width, initial-scale=1'
      />
      <meta httpEquiv="content-language" content="es" />
      <title>Las Mejores Ofertas en Vuelos | Xvuelos</title>
      <link rel='canonical' href='https://www.xvuelos.com' />
      <link href="https://fonts.googleapis.com/css?family=Open+Sans|Lato:400,700,900" rel="stylesheet"/>
      <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
      <link rel="icon" href="/favicon.ico" type="image/x-icon" />
    </Helmet>
    <Router>
      <PageContainer>
        <Header>
          <Logo />
          <BlogLink />
        </Header>
        <MainContainer>
          <React.Suspense fallback={<Spinner />}>
            <Switch>
              <Route exact path='/'><SearchForm /></Route>
              <Route path='/vuelos/:origin/:destination/:fromDay/:toDay/:itineraryId?'>
                <Flights />
              </Route>
              <Route path='/quienes-somos'><QuienesSomos /></Route>
              <Route path='/layout'><Layout /></Route>
              <Route path='/debug'><Debug /></Route>
            </Switch>
          </React.Suspense>
        </MainContainer>
        <Footer>
          <UserInfo />
        </Footer>
      </PageContainer>
    </Router>
  </StyleProvider>
);

export default App;
