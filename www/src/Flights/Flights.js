import React, { useState, useEffect, useCallback, useMemo } from 'react';
import { useHistory, useLocation, useRouteMatch } from 'react-router-dom';
import styled from 'styled-components';
import { Button, KIND } from 'baseui/button';
import ChevronLeft from 'baseui/icon/chevron-left';
import FilterIcon from 'baseui/icon/filter';

import ProgressBar from './ProgressBar';

import { ResponsiveContainer, Fullscreen, OnlyDesktop, OnlyMobile, HidableContainer } from '../components/Layout'
import { getSession, getPollResults } from '../api';
import { FlightsContext } from '../contexts';
import FlightsList from './FlightsList';
import ItineraryDetail from './ItineraryDetail';
import Filters from './Filters';
import { useIsDebug, useUserInfo } from '../hooks';
import { PAGESIZE, RESPONSE_STATUS, RETRY_INTERVAL, URL_HASH } from '../constants';
import filterFlights from './Filters/util/filterFlights';

const FiltersWrapper = styled.div`
  max-width: 320px;
  margin-right: 32px;
  flex: 1;
`;

const Flights = () => {
  const match = useRouteMatch();
  const history = useHistory();
  const location = useLocation();
  const { origin, fromDay, destination, toDay, itineraryId } = match.params;
  const [ isLoading, setLoading ] = useState(false);
  const [ results, setResults ] = useState({});
  const [ listSize, setListSize ] = useState(PAGESIZE);
  const [ isDebug ] = useIsDebug();
  const [ userInfo ] = useUserInfo();
  const [ filters, setFilters ] = useState([]);

  const handleFailure = () => {
    setLoading(false);
  };
  
  const handlePollResults = useCallback((lastResponse) => {
    setResults(lastResponse);
    if (lastResponse.Status === RESPONSE_STATUS.UPDATES_PENDING) {
      setTimeout(() => getPollResults({
          SessionKey: lastResponse.SessionKey,
          isDebug
        }).then(handlePollResults).catch(handleFailure), RETRY_INTERVAL);
    } else if (lastResponse.Status === RESPONSE_STATUS.UPDATES_COMPLETE) {
      setLoading(false);
    }
  }, [isDebug]);

  useEffect(() => {
    const { market, currency, locale } = userInfo;

    setLoading(true);
    setListSize(PAGESIZE)
    
    getSession({
      market,
      currency,
      locale,
      originPlace: origin,
      destinationPlace: destination,
      outboundDate: fromDay,
      inboundDate: toDay,
      isDebug
    })
    .then(handlePollResults)
    .catch(handleFailure);
  }, [origin, destination, fromDay, toDay, isDebug, userInfo, handlePollResults, setListSize]);

  useEffect(() => {
    setListSize(PAGESIZE)
  }, [filters]);

  const {
    SessionKey,
    Query,
    Status,
    Itineraries = [],
    Legs,
    Segments,
    Carriers,
    Agents = [],
    Places,
    Currencies
  } = results;

  const filteredFlights = useMemo(() => filterFlights({ results, filters })
    , [results, filters]);
  const flights = filteredFlights.slice(0, listSize);

  const handleFilterOpen = () => {
    const { pathname } = location;
    history.push(`${pathname}${URL_HASH.FILTERS}`);
  }

  const handleFilterClose = () => {
    const { pathname } = location;
    history.replace(pathname);
  }

  const handleSelectFlight = (id) => {
    const { pathname } = location;
    history.push(`${pathname}/${id}`);
  }

  const handleFiltersChange = (newFilters) => {
    setFilters(newFilters);
  }

  const handleShowMore = () => {
    setListSize(listSize + PAGESIZE);
  }

  const { hash } = location;
  const isFiltering = hash === URL_HASH.FILTERS;
  const isShowingResults = !isFiltering && !itineraryId;
  const isShowMoreDisabled = listSize >= Itineraries.length;

  return (
    <FlightsContext.Provider
      value={{
        onSelectFlight: handleSelectFlight,
        onFiltersChange: handleFiltersChange,
        filters,
        SessionKey,
        Query,
        Status,
        Itineraries,
        Legs,
        Segments,
        Carriers,
        Agents,
        Places,
        Currencies,
        isLoading,
        flights
      }}
    >
      {isFiltering && (
        <Fullscreen>
          <Button
            onClick={handleFilterClose}
            kind={KIND.minimal}
            startEnhancer={ChevronLeft}
          >
            Cerrar
          </Button>

          <Filters />
        </Fullscreen>
      )}

      {itineraryId && <ItineraryDetail />}

      <HidableContainer hidden={!isShowingResults}>
        <OnlyMobile>
          <Button
            onClick={handleFilterOpen}
            kind={KIND.minimal}
            endEnhancer={FilterIcon}
          >
            Filtrar
          </Button>
        </OnlyMobile>

        <Button
          onClick={() => history.push('/')}
          kind={KIND.minimal}
          startEnhancer={ChevronLeft}
        >
          Modificar
        </Button>

        <h2>{filteredFlights.length} Resultados</h2>

        <ProgressBar />

        <ResponsiveContainer>
          <OnlyDesktop>
            <FiltersWrapper>
              <Filters />
            </FiltersWrapper>
          </OnlyDesktop>
          <FlightsList>
            <Button
              onClick={handleShowMore}
              kind={KIND.minimal}
              isLoading={isLoading}
              disabled={isShowMoreDisabled}
            >
              Mostrar más vuelos
            </Button>
          </FlightsList>
        </ResponsiveContainer>
      </HidableContainer>
    </FlightsContext.Provider>
  );
}

export default Flights;
