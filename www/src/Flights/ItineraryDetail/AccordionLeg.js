import React from 'react';
import styled from 'styled-components';
import { Accordion, Panel } from 'baseui/accordion';

import { toStringDate, minsToHHMM } from '../../util';
import Leg from '../FlightCard/Leg';

import SegmentDetail from './SegmentDetail';
import Stop from './Stop';

const LegWrapper = styled.div`
  width: 100%;
  margin-right: 10px;
`;

const LegHeader = ({
  directionality,
  date
}) => (
    <div style={{ display: 'flex' }}>
      <h4 style={{ marginBottom: '10px' }}>
        {directionality === 'Outbound'
          ? 'Vuelo de ida'
          : 'Regreso'
        }
      </h4>

      <h6
        style={{
          color: '#68697f',
          marginBottom: '10px',
          marginLeft: '6px'
        }}
      >
        {toStringDate(date)}
      </h6>
    </div>
  );

const ArrivalDuration = ({
  date,
  mins
}) => (
    <div
      style={{
        marginTop: '40px'
      }}
    >
      <small>
        <strong>Llegada:</strong><span>{toStringDate(date)} | </span>
        <strong>Duración del viaje:</strong><span>{minsToHHMM(mins)}</span>
      </small>
    </div>
  );

var borderStyle = '.375rem';

const AccordionLeg = ({
  leg
}) => (
    <>
      <LegHeader
        directionality={leg.Directionality}
        date={leg.Departure}
      />
      <Accordion
        onChange={({ expanded }) => {
          borderStyle = expanded.length === 0 ? '.375rem' : '.375rem .375rem 0 0';
        }}
        overrides={{
          Header: {
            style: ({ $theme }) => ({
              border: '1px solid #ccc',
              backgroundColor: $theme.colors.mono100,
              borderRadius: borderStyle,
              outline: '0'
            })
          },
          Content: {
            style: ({ $theme }) => ({
              backgroundColor: $theme.colors.mono100,
              borderLeft: '1px solid #cccccc',
              borderRight: '1px solid #cccccc'
            })
          }
        }}
      >
        <Panel
          title={(
            <LegWrapper>
              <Leg id={leg.Id} />
            </LegWrapper>
          )}
        >
          {leg.SegmentIds.map((id, index) => (
            <div key={id}>
              {index > 0 && (
                <pre>
                  <Stop
                    arrivallId={leg.SegmentIds[index - 1]}
                    departureId={leg.SegmentIds[index]}
                  />
                </pre>
              )}
              <SegmentDetail id={id} />
            </div>
          ))}
          <ArrivalDuration
            date={leg.Arrival}
            mins={leg.Duration}
          />
        </Panel>
      </Accordion>
    </>
  );

export default AccordionLeg;