import React, { useContext } from 'react';
import { useRouteMatch } from 'react-router-dom';
import styled from 'styled-components';

import { winScrollTop } from '../../util';
import { FlightsContext } from '../../contexts';
import { ResponsiveContainer, RightBar } from '../../components/Layout';

import PricingCard from './PricingCard/index';
import AccordionLeg from './AccordionLeg';

const MainContainer = styled.div`
  min-width: 60%;
`;

const ItineraryDetail = () => {
  winScrollTop()
  const match = useRouteMatch();

  const { itineraryId } = match.params;
  const { flights = [], Legs = [] } = useContext(FlightsContext);
  const flight = flights.find(f => f.id === itineraryId);

  if (!flight) {
    return (
      <span>Cargando Resultados</span>
    )
  } else {
    const { OutboundLegId, InboundLegId } = flight;
    const outBoundLeg = Legs.find(l => l.Id === OutboundLegId);
    const inBoundLeg = Legs.find(l => l.Id === InboundLegId);
    const { PricingOptions = [] } = flight;

    return (
      <ResponsiveContainer>
        <MainContainer>
          <AccordionLeg leg={outBoundLeg} />
          <AccordionLeg leg={inBoundLeg} />
          <h2>Reserva tu boleto</h2>
          {PricingOptions.map((pricingOption, idx) => (
            <PricingCard key={idx} pricingOption={pricingOption} />
          ))}
        </MainContainer>
        <RightBar>

        </RightBar>
      </ResponsiveContainer>
    );
  }
};

export default ItineraryDetail;
