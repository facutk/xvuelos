import React ,{useContext} from 'react';
import { msToTime } from '../../util/msToHHMM';
import { FlightsContext } from '../../contexts';
import styled from 'styled-components';

const StopWrapper = styled.div`
    position: relative;
    display: flex;
    margin: .75rem -.75rem;
    padding: .75rem;
    border-radius: .375rem;
    background: #f1f2f8;
    color: #d1435b;
`;


const Stop = ({arrivallId,departureId}) =>{
  
    const {Segments = []}= useContext(FlightsContext);
    const arrivalSegment = Segments.find(s => s.Id === arrivallId);
    const departureSegment = Segments.find(s => s.Id === departureId);
    
    const stopDuration = msToTime(new Date(departureSegment.DepartureDateTime) - new Date(arrivalSegment.ArrivalDateTime))
     
    return (
         <StopWrapper> 
         {stopDuration} <span> Escala en aeropuerto</span>
         </StopWrapper>
        
    )
}

export default Stop


