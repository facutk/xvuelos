import React, { useContext } from 'react';
import styled from 'styled-components';
import { ProgressSteps, Step } from 'baseui/progress-steps';
import { StatefulTooltip, TRIGGER_TYPE } from 'baseui/tooltip';

import { FlightsContext } from '../../contexts';
import { minsToHHMM, datetimeToHHMM } from '../../util';
import { MOBILE_ONLY } from '../../constants';

const TimeStationWrapper = styled.div`
  display:flex;

  @media ${MOBILE_ONLY} {
    display:none;
  }
`;

const MobileTimeStation = styled.div`
  display:none;

  @media ${MOBILE_ONLY} {
    display:flex;
    margin-left: 5px;
    max-width: 180px;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
  };
`;

const HideOnDesktop = styled.div`
  display:none;

  @media ${MOBILE_ONLY} {
    display:block
  };
`;

const Station = ({
  id,
  hideName
}) => {
  const { Places = [] } = useContext(FlightsContext);

  const station = Places.find(p => p.Id === id);

  const { Code, Name } = station;

  return (
    <div style={{ marginLeft: "5px" }}>
      {Code} {hideName ? '' : <small>{Name}</small>}
    </div>
  );
};

const TimeStation = ({
  date,
  stationId
}) => (
    <>
      <HideOnDesktop>
        <StatefulTooltip
          content={() => (
            <div style={{ display: "flex" }}>
              <Station id={stationId} />
            </div>
          )}
          triggerType={TRIGGER_TYPE.click}
          returnFocus
          autoFocus
        >
          <MobileTimeStation>
            {datetimeToHHMM(date)} <Station id={stationId} hideName={true} />
          </MobileTimeStation>
        </StatefulTooltip>
      </HideOnDesktop>

      <TimeStationWrapper>
        {datetimeToHHMM(date)} <Station id={stationId} />
      </TimeStationWrapper>
    </>
  );

const Duration = ({
  min
}) => (
    <small>
      {minsToHHMM(min)}
    </small>
  );

const SegmentDetail = ({
  id
}) => {
  const { Segments = [] } = useContext(FlightsContext);
  const segment = Segments.find(s => s.Id === id);

  return (
    <div
      style={{
        display: 'flex',
        alignItems: 'center'
      }}
    >
      <Duration min={segment.Duration} />

      <div
        style={{
          maxHeight: '85px'
        }}
      >
        <ProgressSteps current={-1} >
          <Step
            title={
              <TimeStation
                date={segment.DepartureDateTime}
                stationId={segment.OriginStation}
              />
            }
          />
          <Step
            title={
              <TimeStation
                date={segment.ArrivalDateTime}
                stationId={segment.DestinationStation}
              />
            }
          />
        </ProgressSteps>
      </div>
    </div>
  );
};

export default SegmentDetail