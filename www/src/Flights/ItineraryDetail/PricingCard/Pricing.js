import React from 'react';
import styled from 'styled-components';
import { Button, KIND, SIZE } from 'baseui/button';
import ArrowRight from 'baseui/icon/arrow-right';

import Currency from '../../Currency';
import { MOBILE_BREAKPOINT } from '../../../constants';

const Price = styled.div`
  font-size: 1.25em;
  font-weight: bold;
`;

const PriceContainer = styled.div`
  align-self: flex-end;
`;

export const PricingWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  margin-left: 32px;

  @media ${MOBILE_BREAKPOINT} {
    flex-flow: row;
    justify-content: space-between;
    margin-left: 0;
    margin-top: 16px;
  }
`;


const Pricing = ({ price, deeplinkUrl }) => {
  const handleOpenURL = () => window.open(deeplinkUrl);

  return (
    <PricingWrapper style={{ alignItems: 'flex-end' }}>

      <PriceContainer style={{ marginRight: '10px' }}>
        <Price>
          <Currency>
            {price}
          </Currency>
        </Price>
      </PriceContainer>

      <div>
        <Button
          endEnhancer={() => <ArrowRight size={24} />}
          kind={KIND.primary}
          size={SIZE.compact}
          style={{ width: '100%' }}
          onClick={handleOpenURL}
        >
          Seleccionar
        </Button>
      </div>

    </PricingWrapper>
  );
};

export default Pricing