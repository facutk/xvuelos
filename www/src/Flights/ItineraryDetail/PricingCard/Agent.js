import React, { useContext } from 'react';
import styled from 'styled-components';

import { FlightsContext } from '../../../contexts';

const AgentWrapper = styled.div`
  display:flex;
  align-items: center;
`;

const Agent = ({ id }) => {
  const { Agents = [] } = useContext(FlightsContext);
  const Agent = Agents.find(a => a.Id === id);

  return (
    <AgentWrapper>
      <img
        src={Agent.ImageUrl}
        width='90px'
        alt={Agent.Name}
      />
    </AgentWrapper>

  )
}

export default Agent