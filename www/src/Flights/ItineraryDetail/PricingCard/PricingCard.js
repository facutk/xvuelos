import React  from 'react';
import { Segment } from '../../../components/Layout';
import Agent from  './Agent';
import Pricing from  './Pricing';
import styled from 'styled-components';

const CardContainer = styled.div`
justify-content: space-between;
display: flex;
flex-flow: row;
`;

const PricingCard = ({pricingOption}) =>{



    return (
        <Segment>
           <CardContainer>
           <Agent id={pricingOption.Agents[0]} />
           <Pricing price={pricingOption.Price} deeplinkUrl={pricingOption.DeeplinkUrl} /> 
           </CardContainer> 
        </Segment>
         
    )
}

export default PricingCard;