import React, { useContext } from 'react';
import styled from 'styled-components';
import { StyledSpinnerNext as Spinner } from 'baseui/spinner';

import { FlightsContext } from '../contexts';
import { VerticalList } from '../components/Layout'

const FlightCard = React.lazy(() => import('./FlightCard'));

const WiderVerticalList = styled(VerticalList)`
  flex: 3;
`;

const FlightsList = ({ children }) => {
  const { flights = [] } = useContext(FlightsContext);

  return (
    <WiderVerticalList>
      <React.Suspense fallback={<Spinner />}>
        {flights.map(flight => (
          <FlightCard
            key={flight.BookingDetailsLink.Body}
            id={flight.id}
          />
        ))}
        {children}
      </React.Suspense>
    </WiderVerticalList>
  );
}

export default FlightsList;
