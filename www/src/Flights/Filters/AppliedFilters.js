import React, { useContext } from 'react';
import { Tag } from 'baseui/tag';

import { FlightsContext } from '../../contexts';

const AppliedFilters = () => {
  const { filters = [], onFiltersChange } = useContext(FlightsContext);

  const handleRemoveFilter = (toBeRemovedFilter) => {
    const newFilters = filters
      .filter(filter => (
        !(filter.type === toBeRemovedFilter.type
          && filter.value === toBeRemovedFilter.value))
      );
    onFiltersChange(newFilters);
  };

  return (
    <>
      {filters.map(filter => (
        <Tag
          key={filter.value}
          onActionClick={() => handleRemoveFilter(filter)}
        >
          {filter.label}
        </Tag>
      ))}
    </>
  );
}

export default AppliedFilters;
