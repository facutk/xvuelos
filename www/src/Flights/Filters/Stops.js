import React, { useContext } from 'react';
import { Radio, RadioGroup } from 'baseui/radio';

import { FlightsContext } from '../../contexts';
import { FILTER_TYPES, STOP_OPTIONS, DEFAULT_STOP_OPTION_VALUE } from './constants';

const Stops = () => {
  const { filters = [], onFiltersChange } = useContext(FlightsContext);

  const handleSetStops = (newStopValue) => {
    let newFilters = filters.filter(filter => filter.type !== FILTER_TYPES.STOPS);

    if (newStopValue !== DEFAULT_STOP_OPTION_VALUE) {
      const selectedOption = STOP_OPTIONS.find(stopOption => stopOption.value === newStopValue);

      newFilters = newFilters.concat({
        type: FILTER_TYPES.STOPS,
        value: selectedOption.value,
        label: selectedOption.label,
        stops: selectedOption.stops
      })
    }

    onFiltersChange(newFilters);
  }

  const selectedStopsFilter = filters.find(filter => filter.type === FILTER_TYPES.STOPS) || {};
  const { value = DEFAULT_STOP_OPTION_VALUE } = selectedStopsFilter;

  return (
    <RadioGroup
      onChange={e => handleSetStops(e.target.value)}
      value={value}
    >
      {STOP_OPTIONS.map(stopOption => (
        <Radio
          key={stopOption.value}
          value={stopOption.value}
        >
          {stopOption.label}
        </Radio>
      ))}
    </RadioGroup>
  );
}

export default Stops;
