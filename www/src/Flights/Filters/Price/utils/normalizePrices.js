const normalizePrices = (min, max, steps) => {
  const range = max - min;
  const gap = Math.ceil(range / steps);
  const gapLen = Math.ceil(Math.log10(gap + 1));
  const gapDivider = Math.pow(10, gapLen - 1);
  const normalizedGap = Math.ceil(gap / gapDivider) * gapDivider;
  const newMin = Math.ceil(min / normalizedGap) * normalizedGap;
  const newMax = Math.ceil(max / normalizedGap) * normalizedGap;

  return {
    newMin,
    newMax,
    normalizedGap
  }
};

export default normalizePrices;
