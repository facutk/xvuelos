const getPriceBounds = (Itineraries = []) => {
  let minPrice = 0;
  let maxPrice = 1;

  if (!Itineraries.length) {
    return [minPrice, maxPrice];
  }

  minPrice = Number.MAX_VALUE;
  maxPrice = Number.MIN_VALUE;

  for (const itinerary of Itineraries) {
    for (const pricingOption of itinerary.PricingOptions) {
      if (pricingOption.Price < minPrice) {
        minPrice = pricingOption.Price;
      }
      if (pricingOption.Price > maxPrice) {
        maxPrice = pricingOption.Price;
      }
    }
  }

  return [minPrice, maxPrice];
};

export default getPriceBounds;
