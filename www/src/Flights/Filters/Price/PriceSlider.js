import React from 'react';
import { Slider } from 'baseui/slider';
import { useStyletron } from 'baseui';

const PriceSlider = ({
  min,
  max,
  step,
  value,
  disabled,
  onChange
}) => {
  const [css, theme] = useStyletron();

  return (
    <Slider
      min={min}
      max={max}
      step={step}
      value={[value]}
      disabled={disabled}
      onChange={({ value }) => onChange(value[0])}
      overrides={{
        ThumbValue: () => null,
        TickBar: () => (
          <div
            className={css({
              paddingLeft: theme.sizing.scale600,
              paddingBottom: theme.sizing.scale400,
              ...theme.typography.font200,
              color: theme.colors.mono700,
              ...theme.typography.font350
            })}
          >
            {value >= max && 'Cualquier precio'}
            {value < max && `Menos de ${value}`}
          </div>
        )
      }}
    />
  );
}

export default PriceSlider;
