import React, { useContext, useMemo } from 'react';

import { FlightsContext } from '../../../contexts';
import PriceSlider from './PriceSlider';
import { getPriceBounds, normalizePrices } from './utils';
import { FILTER_TYPES } from '../constants';

const PRICE_MAX_STEPS = 15;

const Price = () => {
  const { Itineraries = [], filters = [], onFiltersChange } = useContext(FlightsContext);

  const {
    newMin,
    newMax,
    normalizedGap,
    maxSelectedPrice
  } = useMemo(() => {
    const [minPrice, maxPrice] = getPriceBounds(Itineraries);

    const {
      newMin,
      newMax,
      normalizedGap
    } = normalizePrices(minPrice, maxPrice, PRICE_MAX_STEPS);

    const priceFilter = filters.find(filter => filter.type === FILTER_TYPES.PRICE) || {};
    let maxSelectedPrice = priceFilter.value || newMax;

    if (maxSelectedPrice < newMin || maxSelectedPrice > newMax) {
      maxSelectedPrice = newMax;
    }

    return {
      newMin,
      newMax,
      normalizedGap,
      maxSelectedPrice
    }
  }, [Itineraries, filters]);

  const handleChange = (newValue) => {
    let newFilters = filters.filter(filter => filter.type !== FILTER_TYPES.PRICE);

    if (newValue < newMax) {
      newFilters = newFilters.concat({
        type: FILTER_TYPES.PRICE,
        value: newValue,
        label: `Menos de ${newValue}`
      });
    }

    onFiltersChange(newFilters);
  };

  return (
    <PriceSlider
      min={newMin}
      max={newMax}
      step={normalizedGap}
      value={maxSelectedPrice}
      onChange={handleChange}
      disabled={!Itineraries.length}
    />
  );
}

export default Price;
