import React, { useContext } from 'react';
import { Checkbox } from 'baseui/checkbox';

import { FlightsContext } from '../../../contexts';
import { FILTER_TYPES } from '../constants';

const Airlines = () => {
  const {
    Carriers = [],
    filters = [],
    onFiltersChange
  } = useContext(FlightsContext);

  const airlinesFilters = filters.filter(filter => filter.type === FILTER_TYPES.AIRLINES);

  const checkedAirlines = airlinesFilters.reduce((acc, f) => {
    acc[f.value] = true;
    return acc;
  }, {});

  const handleAirlineFilterChange = (e) => {
    let newFilters;

    const clickedAirlineId = +e.target.value;
    const clickedAirline = Carriers.find(carrier => carrier.Id === clickedAirlineId) || {};

    if (checkedAirlines[clickedAirlineId]) {
      newFilters = filters
        .filter(filter => !(filter.type === FILTER_TYPES.AIRLINES
          && filter.value === clickedAirlineId));
    } else {
      newFilters = filters.concat({
        type: FILTER_TYPES.AIRLINES,
        value: clickedAirlineId,
        label: clickedAirline.Name
      });
    }

    onFiltersChange(newFilters);
  }

  const handleClearAirlineFilters = () => {
    const newFilters = filters.filter(filter => filter.type !== FILTER_TYPES.AIRLINES);

    onFiltersChange(newFilters);
  }

  const isAllAirlinesChecked = airlinesFilters.length === 0;

  return (
    <>
      <Checkbox
        checked={isAllAirlinesChecked}
        onChange={handleClearAirlineFilters}
      >
        Todas las aerolíneas
      </Checkbox>

      {Carriers.map(carrier => (
        <Checkbox
          key={carrier.Id}
          onChange={handleAirlineFilterChange}
          value={carrier.Id}
          checked={checkedAirlines[carrier.Id]}
        >
          {carrier.Name}
        </Checkbox>
      ))}
    </>
  );
}

export default Airlines;
