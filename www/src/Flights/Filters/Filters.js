import React from 'react';
import { VerticalList } from '../../components/Layout';

import AppliedFilters from './AppliedFilters';
import Stops from './Stops';
import Price from './Price';
import Airlines from './Airlines';

const Filters = () => {
  return (
    <VerticalList>
      <AppliedFilters />

      <h3>Escalas</h3>
      <Stops />
      
      <h3>Precio</h3>
      <Price />

      <h3>Aerolíneas</h3>
      <Airlines />
    </VerticalList>
  );
}

export default Filters;
