const STOP_OPTIONS = [
  {
    label: 'Todas',
    value: 'ANY'
  },
  {
    label: 'Directo',
    value: 'NON_STOP',
    stops: 0
  },
  {
    label: 'Una o menos',
    value: 'ONE_OR_LESS',
    stops: 1
  },
  {
    label: 'Dos o menos',
    value: 'TWO_OR_LESS',
    stops: 2
  }
];

export const DEFAULT_STOP_OPTION_VALUE = STOP_OPTIONS[0].value;

export default STOP_OPTIONS;
