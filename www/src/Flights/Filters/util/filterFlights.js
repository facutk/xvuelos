import { FILTER_TYPES } from '../constants';

const filterFlights = ({
  results = {},
  filters = []
}) => {
  const {
    Itineraries = [],
    Legs = []
  } = results;

  if (filters.length) {
    const StopsFilter = filters.find(filter => filter.type === FILTER_TYPES.STOPS);
    const PriceFilter = filters.find(filter => filter.type === FILTER_TYPES.PRICE);
    const AirlineFilter = filters.filter(filter => filter.type === FILTER_TYPES.AIRLINES);

    const FilteredItineraries = Itineraries.map(itinerary => {
      const { InboundLegId, OutboundLegId, PricingOptions = [] } = itinerary;

      const InboundLegInfo = Legs.find(leg => leg.Id === InboundLegId);
      const OutboundLegInfo = Legs.find(leg => leg.Id === OutboundLegId);
      const stops = Math.max(InboundLegInfo.Stops.length, OutboundLegInfo.Stops.length);
      const price = Math.max(...PricingOptions.map(PricingOption => PricingOption.Price));

      return {
        ...itinerary,
        stops,
        price,
        carriers: [].concat(InboundLegInfo.Carriers, OutboundLegInfo.Carriers)
      };
    }).filter(itinerary => {
      if (StopsFilter) {
        return itinerary.stops <= StopsFilter.stops;
      }

      return true;
    }).filter(itinerary => {
      if (PriceFilter) {
        if (itinerary.price > PriceFilter.value) return false;
      }

      return true;
    })
    .filter(itinerary => {
      if (AirlineFilter.length) {
        const selectedAirlines = AirlineFilter.map(filter => filter.value);
        return itinerary.carriers.every(carrier => selectedAirlines.includes(carrier));
      }

      return true;
    });

    return FilteredItineraries;
  }

  return Itineraries;
};

export default filterFlights;
