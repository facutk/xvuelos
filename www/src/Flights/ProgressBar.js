import React, { useContext, useMemo } from 'react';
import { FlightsContext } from '../contexts';
import { ProgressBar as BaseuiProgressBar } from 'baseui/progress-bar';

import { RESPONSE_STATUS } from '../constants';

const ProgressBar = () => {
  const { Agents = [], isLoading } = useContext(FlightsContext);
  const totalAgents = Agents.length;
  const loadedAgents = useMemo(() =>
    Agents.filter(agent => agent.Status === RESPONSE_STATUS.UPDATES_COMPLETE).length
  , [Agents]);
  const delta = !!totalAgents ? 0 : 1;

  return (
    isLoading ? <BaseuiProgressBar
      value={loadedAgents}
      successValue={totalAgents + delta}
    /> : <div style={{ margin: '36px' }} />
  )
};

export default ProgressBar;
