import React, { useContext } from 'react';
import { FlightsContext } from '../contexts';

const Currency = ({ children }) => {
  const { Query = {} } = useContext(FlightsContext);
  const { Currency, Locale } = Query;

  const formatedCurrency = children.toLocaleString(Locale, {
    style: 'currency',
    currency: Currency,
    minimumFractionDigits: 0,
    maximumFractionDigits: 0
  });

  return (
    <React.Fragment>{ formatedCurrency }</React.Fragment>
  );
}

export default Currency;
