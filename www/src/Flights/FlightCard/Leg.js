import React, { useContext } from 'react';
import styled from 'styled-components';

import { Styles } from '../../components/Layout';
import { FlightsContext } from '../../contexts';
import { useIsDebug } from '../../hooks';
import { minsToHHMM, datetimeToHHMM, dateDiff } from '../../util';

import TinyTimeline from './TinyTimeline';

const StationWrapper = styled.div`
  font-size: 0.85em;
  color: ${Styles.colors.mono800};
`;

const Station = ({ id }) => {
  const { Places = [] } = useContext(FlightsContext);
  
  const station = Places.find(p => p.Id === id);

  const { Code } = station;

  return <StationWrapper>{Code}</StationWrapper>;
};

const TimeWrapper = styled.div`
  font-size: 1.15em;
  font-weight: 600;
`;

const Time = ({ datetime }) => <TimeWrapper>{datetimeToHHMM(datetime)}</TimeWrapper>;

const SEPARATOR = '/';
const miniImage = (imageUrl) => {
  const path = imageUrl.split(SEPARATOR);
  const logo = path.pop();
  path.push('favicon');
  path.push(logo);

  return path.join(SEPARATOR);
};

const CarrierWrapper = styled.div`
  margin-right: 6px;
  margin-top: 3px;
`;

const Carrier = ({ id }) => {
  const [ isDebug ] = useIsDebug();

  const { Carriers = [] } = useContext(FlightsContext);
  
  const carrier = Carriers.find(c => c.Id === id);

  const { Name, ImageUrl } = carrier;

  let miniImageUrl = miniImage(ImageUrl);
  if (isDebug) {
    miniImageUrl = 'https://via.placeholder.com/32';
  }

  return (
    <CarrierWrapper>
      <object
        data={miniImageUrl}
        type='image/png'
        width='32px'
        height='32px'
      >
        <img
          src='https://via.placeholder.com/32'
          width='32px'
          height='32px'
          alt={Name}
        />
      </object>
    </CarrierWrapper>
  );
};

const DurationWrapper = styled.div`
  font-size: 0.85em;
`;

const StopsWrapper = styled.div`
  font-size: 0.85em;
`;

const LegWrapper = styled.div`
  display: flex;
  margin-top: 16px;
  justify-content: space-between;
  flex-grow: 1;

  &:first-child {
    margin-top: 0;
  }
`;

const LeftWrapper = styled.div`
  display: flex;
  flex-direction: row;
`;

const MiddleWrapper = styled.div`
  text-align: center;
  color: ${Styles.colors.mono800};
`;

const RightWrapper = styled.div`
  text-align: right;
  display: flex;
  flex-direction: row;
`;

const TimeStationWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const OffsetDays = styled.div`
  font-size: 0.85em;
  position: relative;
  top -6px;
  width: 15px;
`;

const Leg = ({ id }) => {
  const { Legs = [] } = useContext(FlightsContext);

  const leg = Legs.find(l => l.Id === id);

  const {
    OriginStation,
    DestinationStation,
    Departure,
    Arrival,
    Duration,
    Stops,
    Carriers
  } = leg;

  const stopsCount = Stops.length;
  const stopsLabel = stopsCount === 0 ?
    'Directo' :
    `${stopsCount} Escala${stopsCount === 1 ? '' : 's'}`;

  const [carrierId] = Carriers;

  const dateDiffInDays = dateDiff(new Date(Departure), new Date(Arrival));

  return (
    <LegWrapper>
      <LeftWrapper>
        <Carrier id={carrierId} />

        <TimeStationWrapper>
          <Time datetime={Departure} />
          <Station id={OriginStation} />
        </TimeStationWrapper>
      </LeftWrapper>

      <MiddleWrapper>
        <StopsWrapper>
          {stopsLabel}
        </StopsWrapper>
        <TinyTimeline Stops={Stops} />
      </MiddleWrapper>

      <MiddleWrapper>
        <DurationWrapper>◷ {minsToHHMM(Duration)}</DurationWrapper>
      </MiddleWrapper>

      <RightWrapper>
        <TimeStationWrapper>
          <Time datetime={Arrival} />
          <Station id={DestinationStation} />
        </TimeStationWrapper>
        <OffsetDays>
          {(dateDiffInDays > 0) && (<span>+{dateDiffInDays}</span>)}
        </OffsetDays>
      </RightWrapper>
    </LegWrapper>
  );
}

export default Leg;
