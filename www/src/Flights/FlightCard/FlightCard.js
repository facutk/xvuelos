import React, { useContext } from 'react';
import styled from 'styled-components';

import { FlightsContext } from '../../contexts';
import { ResponsiveContainer, Segment } from '../../components/Layout';
import Pricing from './Pricing';
import Leg from './Leg';

const FlightInfoWrapper = styled.div`
  flex-grow: 1;
`;

const FlightCard = ({
  id
}) => {
  const { flights = [] } = useContext(FlightsContext);

  const flight = flights
    .find(f => f.id === id);

  const {
    OutboundLegId,
    InboundLegId,
    PricingOptions = []
  } = flight;

  return (
    <Segment>
      <ResponsiveContainer>
        <FlightInfoWrapper>
          <Leg id={OutboundLegId} />
          <Leg id={InboundLegId} />
        </FlightInfoWrapper>

        <Pricing
          PricingOptions={PricingOptions}
          id={id}
        />
      </ResponsiveContainer>
    </Segment>
  );
};

export default FlightCard;
