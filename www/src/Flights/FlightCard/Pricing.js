import React, { useContext } from 'react';
import styled from 'styled-components';
import { Button, KIND, SIZE } from 'baseui/button';
import ArrowRight from 'baseui/icon/arrow-right';

import { MOBILE_BREAKPOINT } from '../../constants';
import { FlightsContext } from '../../contexts';
import Currency from '../Currency';

const OfferLabel = styled.div`
  font-size: 0.75em;
`;

const Price = styled.div`
  font-size: 1.25em;
  font-weight: bold;
`;

const PriceContainer = styled.div`
  align-self: flex-end;
`;

export const PricingWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  margin-left: 32px;

  @media ${MOBILE_BREAKPOINT} {
    flex-direction: row;
    justify-content: space-between;
    margin-left: 0;
    margin-top: 16px;
  }
`;

const Pricing = ({
  PricingOptions = [],
  id = ''
}) => {
  const { onSelectFlight } = useContext(FlightsContext);

  const offersCount = PricingOptions.length;
  const prices = PricingOptions.map(p => p.Price);
  const minPrice = Math.min.apply(Math, prices);

  return (
    <PricingWrapper>
      <PriceContainer>
        {offersCount > 1 && <OfferLabel>{offersCount} ofertas desde</OfferLabel>}
        <Price><Currency>{minPrice}</Currency></Price>
      </PriceContainer>
      <div>
        <Button
          endEnhancer={() => <ArrowRight size={24} />}
          kind={KIND.primary}
          size={SIZE.compact}
          style={{ width: '100%' }}
          onClick={() => onSelectFlight(id)}
        >
          Ver
        </Button>
      </div>
    </PricingWrapper>
  );
};

export default Pricing;
