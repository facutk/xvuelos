import React, { useContext } from 'react';
import { FlightsContext } from '../contexts';

const Agent = ({ id }) => {
  const { Agents = [] } = useContext(FlightsContext);

  const agent = Agents.find(a => a.Id === id);

  return (
    <React.Fragment>
      {agent.Name}
    </React.Fragment>
  );
}

export default Agent;
