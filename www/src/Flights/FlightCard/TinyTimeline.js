import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Styles } from '../../components/Layout';

const StopoverIcon = styled.div`
  height: 6px;
  width: 6px;
  background: ${Styles.colors.background};
  margin-top: -5px;
  border-radius: 6px;
  border: 1px solid ${Styles.colors.mono800};
`;

const Timeline = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  margin-top: 6px;
  margin-bottom: 2px;
  border-top: 1px solid ${Styles.colors.mono800};
`;

const TinyTimeline = ({ Stops }) => (
  <Timeline>
    {Stops.map(stop => <StopoverIcon key={stop} />)}
  </Timeline>
);

TinyTimeline.propTypes = {
  Stops: PropTypes.arrayOf(PropTypes.number)
};

TinyTimeline.defaultProps = {
  Stops: []
};

export default TinyTimeline;
