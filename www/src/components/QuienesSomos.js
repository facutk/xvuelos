import React from 'react';
import { Helmet } from 'react-helmet';

const QuienesSomos = () => (
  <>
    <Helmet>
      <title>Quienes Somos | Xvuelos</title>
    </Helmet>

    <h1>Quienes Somos</h1>
    <p>
      En Xvuelos ayudamos a cumplir tus deseos de viajar, comparando los precios de cientos de aerolíneas y agencias en un mismo lugar para que encuentres la mejor opción de forma fácil y rápida.
    </p>
    <p>
      La oferta deseada podrás reservarla directamente con el proveedor sin intermediarios ni costo adicional.
    </p>
    <p>
      Xvuelos es un metabuscador de vuelos líder en Latinoamérica que actualmente opera en Argentina, Brasil, Chile, Colombia, Ecuador, México y Perú.
    </p>
    <p>
      La herramienta compara de manera transparente cientos de sitios de viajes globales y locales para ayudar a millones de usuarios mensuales a elegir el mejor vuelo para tus viajes.      
    </p>
  </>
);

export default QuienesSomos;
