import React from 'react';
import PropTypes from 'prop-types';
import { Provider as StyletronProvider } from 'styletron-react';
import { Client as Styletron } from 'styletron-engine-atomic';
import { LightTheme, BaseProvider } from 'baseui';
import { createTheme, lightThemePrimitives } from 'baseui';

import { useIsDebug } from '../../hooks';

const engine = new Styletron();

const StyleProvider = ({
  children
}) => {
  const [ isDebug ] = useIsDebug();
  let theme = LightTheme;

  if (isDebug) {
    theme = createTheme({
      ...lightThemePrimitives,
      primary400: '#676767',
      primary500: '#484848',
      primary600: '#393939'
    });
  } else {
    theme = createTheme({
      ...lightThemePrimitives,
      primary: lightThemePrimitives.accent,
      primary700: lightThemePrimitives.accent700,
      primary600: lightThemePrimitives.accent600,
      primary500: lightThemePrimitives.accent500,
      primary400: lightThemePrimitives.accent400,
      primary300: lightThemePrimitives.accent300,
      primary200: lightThemePrimitives.accent200,
      primary100: lightThemePrimitives.accent100,
      primary50: lightThemePrimitives.accent50
    });
  }

  return (
    <StyletronProvider value={engine}>
      <BaseProvider theme={theme}>
        {children}
      </BaseProvider>
    </StyletronProvider>
  );
}

StyleProvider.propTypes = {
  children: PropTypes.node
};

StyleProvider.defaultProps = {
  children: null
}

export default StyleProvider;
