import { styled as baseuiStyled } from 'baseui';;

 const RightBar = baseuiStyled('div', ({$theme}) => ({
    color: $theme.colors.primary,
    backgroundColor: $theme.colors.mono100,
    borderLeft: `1px solid ${$theme.colors.mono300}`,
    padding: $theme.sizing.scale600,
    minWidth: `40%`,
    /* minHeight: `100vh`, */
    marginLeft: `32px`
  }));

  export default RightBar;