import styled from 'styled-components';

const MainContainer = styled.div`
  flex: 1 0 auto;
  max-width: 1024px;
  margin: 0 auto;
  padding: 16px;
  width: calc(100% - 32px);
`;

export default MainContainer;
