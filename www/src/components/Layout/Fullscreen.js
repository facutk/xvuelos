import styled from 'styled-components';

const Fullscreen = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  background-color: white;
  z-index: 9999;
  overflow-y: scroll;
`;

export default Fullscreen;
