import { styled as baseuiStyled } from 'baseui';

const Header = baseuiStyled('div', ({$theme}) => ({
  color: $theme.colors.primary,
  backgroundColor: $theme.colors.mono100,
  border: `1px solid ${$theme.colors.mono300}`,
  padding: $theme.sizing.scale600,
  display: 'flex',
  justifyContent: 'space-between'
}));

export default Header;
