import styled from 'styled-components';
import Styles from './Styles'

const Segment = styled.div`
  border-top: 1px solid ${Styles.colors.mono300};
  padding: 16px 0;

  &:first-child {
    border-top: 0;
  }
`;

export default Segment;
