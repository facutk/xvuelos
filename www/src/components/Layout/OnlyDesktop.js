import React from 'react';
import { useIsMobile } from '../../hooks';

const OnlyDesktop = ({
  children
}) => {
  const isMobile = useIsMobile();

  return ((isMobile === false) && (
    <React.Fragment>
      {children}
    </React.Fragment>
  ));
}

export default OnlyDesktop;
