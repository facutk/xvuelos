import { LightTheme } from 'baseui';

// https://baseweb.design/theming/custom-themes/

// const isDebug = window.localStorage.getItem('isDebug');

const colors = {
  background: LightTheme.colors.background,
  border: LightTheme.colors.border,
  mono300: LightTheme.colors.mono300,
  mono600: LightTheme.colors.mono600,
  mono700: LightTheme.colors.mono700,
  mono800: LightTheme.colors.mono800
};

const typography = {
  font100: {
    fontFamily: 'system-ui, "Helvetica Neue", Helvetica, Arial, sans-serif',
    fontSize: '11px',
    fontWeight: 'normal',
    lineHeigh: '16px',
  }
};

const Styles = {
  colors,
  typography
};

export default Styles;
