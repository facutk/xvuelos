import { styled as baseuiStyled } from 'baseui';

const Footer = baseuiStyled('div', ({$theme}) => ({
  backgroundColor: $theme.colors.mono300,
  padding: $theme.sizing.scale200,
  flexShrink: '0'
}));

export default Footer;
