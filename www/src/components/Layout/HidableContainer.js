import styled from 'styled-components';
import PropTypes from 'prop-types';

const HidableContainer = styled.div`
  ${props => props.hidden && (
    'display: hidden;'
  )}
`;

HidableContainer.propTypes = {
  hidden: PropTypes.bool
};

HidableContainer.defaultProps = {
  hidden: false
};

export default HidableContainer;
