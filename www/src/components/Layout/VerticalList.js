import styled from 'styled-components';

const VerticalList = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`;

export default VerticalList;
