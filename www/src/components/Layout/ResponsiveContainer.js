import styled from 'styled-components';
import { MOBILE_BREAKPOINT } from '../../constants';

const ResponsiveContainer = styled.div`
  display: flex;
  flex-direction: row;
  @media ${MOBILE_BREAKPOINT} {
    flex-direction: column;
  }
`;

export const ResponsiveContainerInverse = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  @media ${MOBILE_BREAKPOINT} {
    flex-direction: row;
  }
`;

export default ResponsiveContainer;
