import React from 'react';
import { useIsMobile } from '../../hooks';

const OnlyMobile = ({
  children
}) => {
  const isMobile = useIsMobile();

  return (isMobile && (
    <React.Fragment>
      {children}
    </React.Fragment>
  ));
}

export default OnlyMobile;
